﻿namespace NumericConversions
{
    public static class ImplicitConversion
    {
        public static long IntToLong(int intValue)
        {
            return intValue;
        }

        public static float IntToFloat(int intValue)
        {
            return intValue;
        }

        public static double IntToDouble(int intValue)
        {
            return intValue;
        }

        public static decimal IntToDecimal(int intValue)
        {
            return intValue;
        }

        public static float LongToFloat(long longValue)
        {
            return longValue;
        }

        public static double LongToDouble(long longValue)
        {
            return longValue;
        }

        public static decimal LongToDecimal(long longValue)
        {
            return longValue;
        }

        public static short ByteToShort(byte byteValue)
        {
            return byteValue;
        }

        public static int ByteToInt(byte byteValue)
        {
            return byteValue;
        }

        // TODO #1: Add a static method here with name "IntToFloat" that gets "intValue" parameter ("int" type) and returns the parameter value that is implicitly converted to "float" type.

        // TODO #2: Add a static method here with name "IntToDouble" that gets "intValue" parameter ("int" type) and returns the parameter value that is implicitly converted to "double" type.

        // TODO #3: Add a static method here with name "IntToDecimal" that gets "intValue" parameter ("int" type) and returns the parameter value that is implicitly converted to "decimal" type.

        // TODO #4: Add a static method here with name "LongToFloat" that gets "longValue" parameter ("long" type) and returns the parameter value that is implicitly converted to "float" type.

        // TODO #5: Add a static method here with name "LongToDouble" that gets "longValue" parameter ("long" type) and returns the parameter value that is implicitly converted to "double" type.

        // TODO #6: Add a static method here with name "LongToDecimal" that gets "longValue" parameter ("long" type) and returns the parameter value that is implicitly converted to "decimal" type.

        // TODO #7: Add a static method here with name "ByteToShort" that gets "byteValue" parameter ("byte" type) and returns the parameter value that is implicitly converted to "short" type.

        // TODO #8: Add a static method here with name "ByteToInt" that gets "byteValue" parameter ("byte" type) and returns the parameter value that is implicitly converted to "int" type.
    }
}
